# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Asignacion(models.Model):
    idusuario = models.OneToOneField('Usuarios', models.DO_NOTHING, db_column='idusuario', primary_key=True)
    idrol = models.ForeignKey('Rol', models.DO_NOTHING, db_column='idrol')

    class Meta:
        
        db_table = 'asignacion'
        unique_together = (('idusuario', 'idrol'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Clientes(models.Model):
    idcliente = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=254)
    apellido = models.CharField(max_length=254)
    ci_nro = models.CharField(max_length=254)
    ruc = models.CharField(max_length=254)
    direccion = models.CharField(max_length=254)
    telefono = models.IntegerField()
    correo = models.CharField(max_length=254)
    estado = models.CharField(max_length=1)
    tipo_cliente = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'clientes'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        
        db_table = 'django_session'


class Estado(models.Model):
    idestado = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=254)
    estado = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'estado'


class Prioridad(models.Model):
    idprioridad = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=254)
    nivel = models.IntegerField()
    estado = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'prioridad'


class Rol(models.Model):
    idrol = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=254)
    estado = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'rol'


class Servicio(models.Model):
    idservicio = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=254)
    estado = models.CharField(max_length=1)
    descripcion = models.CharField(max_length=254)

    class Meta:
        
        db_table = 'servicio'


class Turnos(models.Model):
    idturnos = models.AutoField(primary_key=True)
    fecha = models.DateField()
    hora_llegada = models.TimeField()
    idservicio = models.ForeignKey(Servicio, models.DO_NOTHING, db_column='idservicio')
    idprioridad = models.ForeignKey(Prioridad, models.DO_NOTHING, db_column='idprioridad')
    idestado = models.ForeignKey(Estado, models.DO_NOTHING, db_column='idestado')
    idcliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='idcliente')

    class Meta:
        
        db_table = 'turnos'


class Usuarios(models.Model):
    idusuario = models.AutoField(primary_key=True)
    usuario = models.CharField(max_length=254)
    nombre = models.CharField(max_length=254)
    apellido = models.CharField(max_length=254)
    estado = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'usuarios'
